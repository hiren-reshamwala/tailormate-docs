# Tailormate3d

Tailormate3d is providing service to visualize the product.

Data API and renderer API will help to integrate with your web environment.

Using data API you can get the available data and use those data with renderer API to render the model on viewport.


### Licence

Copyright 2017, 2018 Tailormate3d.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
[http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0.html)

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.