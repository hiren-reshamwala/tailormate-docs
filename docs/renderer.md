To use the renderer API you must include javascript API into your html file.

## Initialize Viewport

    var viewport = new Tailormate3d(options); 

### Parameters

-   `options` **[Object][5]?**
    -   `options.container` **[HTMLElement][7]** `document.getElementById('container')` (optional, default : document.body)
    -   `baseUrl` **[String][1]?** Server URL (Required) - `https://tailormate3d.com/{instanceName}/`
    -   `apikey` **[String][1]?** API Key required to access the data from server (Required)
    -   `options.offsetWidth` **[Number][8]** canvas element width (optional, default : Container element width)
    -   `options.offsetHeight` **[Number][8]** canvas element height (optional, default : Container element height)
    -   `options.shadow` **[Boolean][6]** Enable/Disable 3d object shadow (optional, default `false`)

Returns **ViewportObject** 


### Functions

## resize

Resize the viewport

#### Parameters

-   `width` **[Number][8]?** (Required)
-   `height` **[Number][8]?** (Required)

Example:

    viewport.resize(width,height);

## loadScene

Load 3d scene into the viewport

#### Parameters

-   `sceneReferenceId` **[String][1]?** Reference id of scene (Required) - _find in data API_
-   `options` **[Object][5]?** (required in case of callback)
    -   `options.scale` **[Number][8]** change the scale of object - Float (optional, default : 1)
    -   `options.positionX` **[Number][8]** change the x position of object - Float (optional, default : 1)
    -   `options.positionY` **[Number][8]** change the y position of object - Float (optional, default : 1)
    -   `options.positionZ` **[Number][8]** change the z position of object - Float (optional, default : 1)
-   `callback` **[Function][2]?** called on completion with one arguments `(err, info)`.
	`info` contains the scene data.

Example:

	var options = {};
    viewport.loadScene('spread_collar',options,function(err, info){

    });

Returns **-** 

## getListOfObjectsLoaded

Get list of objects loaded on the viewport

#### Returns

`array` **[Array][9]?** Array of objects

-   `Name` **[String][1]**
-   `Id` **[String][1]**
-   `SceneId` **[String][1]**
-   `Visibility` **[Boolean][6]**

Example:
    
    viewport.getListOfObjectsLoaded();

Sample return:

    [{
		'Name' : 'spread_collar_inner',
		'Id' : 'spread_collar_inner',
		'SceneId': 'spread_collar',
		'Visibility' : true
	},{
		'Name' : 'spread_collar_outer',
		'Id' : 'spread_collar_outer',
		'SceneId': 'spread_collar',
		'Visibility' : true
	}]

## hideScene

Hide scene on the viewport

#### Parameters

-   `sceneId` **[String][1]?** Scene id (Required)

Example:
    
    viewport.hideScene('{sceneId}');


## showScene

Show the hidden scene on the viewport

#### Parameters

-   `sceneId` **[String][1]?** Scene id (Required)

Example:
    
    viewport.showScene('{sceneId}');

## removeScene

Remove scene from the viewport

#### Parameters

-   `sceneId` **[String][1]?** Scene id (Required)

Example:
    
    viewport.removeScene('{sceneId}');


## hideObject

Hide particular scene object on the viewport

#### Parameters

-   `objectId` **[String][1]?** Object id (Required) - _find in getListOfObjectsLoaded function_

Example:
    
    viewport.hideObject('{objectId}');


## showObject

Show the previous hidden scene object on the viewport

#### Parameters

-   `objectId` **[String][1]?** Object id (Required) - _find in getListOfObjectsLoaded function_

Example:
    
    viewport.showObject('{objectId}');

## removeObject

Remove object from the viewport

#### Parameters

-   `objectId` **[String][1]?** Object id (Required) - _find in getListOfObjectsLoaded function_

Example:
    
    viewport.removeObject('{objectId}');
    


## flipBack

Show the back part of the scene

Example:

    viewport.flipBack();

## resetPosition

Reset the position of all scenes loaded on viewport

Example:

    viewport.resetPosition();


## loadTextureBySceneId

Render the texture on the scene _(Find the texture in designs data API)_

#### Parameters

-   `imageUrl` **[String][1]?** Design image url `https://tailormate3d.com/{instanceName}/texture/{referenceId}`
-   `sceneId` **[String][1]?** Scene id
-   `options` **[Object][5]?** (required in case of callback)
    -   `options.shininess` **[Number][8]** Increase/Decrease shine - float 1 to 1000 (optional, default : 1)
    -   `options.repeatSet` **[Number][8]** Repeat image on model - float 0.01 to 100 (optional, default : 1)
    -   `options.transparency` **[Number][8]** change the transparency of model - float 0 to 1 (optional, default : 1)
-   `onSuccess` **[Function][2]?** called on completion with no argument.
-   `onError` **[Function][2]?** called on fail to load with one argument `(error)`. **[Error][3]?**
-   `onProgress` **[Function][2]?** called on progress to load with one argument `(progress)`. **[Progress][8]?**

Example:

	var url = 'https://tailormate3d.com/{instanceName}/texture/{referenceId}';
	var options = {
		repeatSet: 5
	};
    viewport.loadTextureBySceneId(url,'spread_collar',options,function(){
    	//Call on success	
	}, function(error){
		//Call on error
	},function(progress){
        //Call on progress
    });


## loadTextureByName

Render the texture on specific scene object _(Find the texture in designs data API)_

#### Parameters

-   `imageUrl` **[String][1]?** Design image url `https://tailormate3d.com/{instanceName}/texture/{referenceId}`
-   `sceneId` **[String][1]?** Object id
-   `options` **[Object][5]?** (required in case of callback)
    -   `options.shininess` **[Number][8]** Increase/Decrease shine - float 1 to 1000 (optional, default : 1)
    -   `options.repeatSet` **[Number][8]** Repeat image on model - float 0.01 to 100 (optional, default : 1)
    -   `options.transparency` **[Number][8]** change the transparency of model - float 0 to 1 (optional, default : 1)
-   `onSuccess` **[Function][2]?** called on completion with no argument.
-   `onError` **[Function][2]?** called on fail to load with one argument `(error)`. **[Error][3]?**
-   `onProgress` **[Function][2]?** called on progress to load with one argument `(progress)`. **[Progress][8]?**

Example:

	var url = 'https://tailormate3d.com/{instanceName}/texture/{referenceId}';
	var options = {
		repeatSet: 5
	};
    viewport.loadTextureByName(url,'spread_collar',options,function(){
    	//Call on success	
	}, function(error){
		//Call on error
	},function(progress){
        //Call on progress
    });


## loadColorBySceneId

Render the RGB color on the scene

#### Parameters

-   `r` **[Number][8]?** Red color of RGB - Integer 0 to 255 (required)
-   `g` **[Number][8]?** Green color of RGB - Integer 0 to 255 (required)
-   `b` **[Number][8]?** Blue color of RGB - Integer 0 to 255 (required)
-   `sceneId` **[String][1]?** Scene id
-   `options` **[Object][5]?** (required in case of callback)
    -   `options.shininess` **[Number][8]** Increase/Decrease shine - float 1 to 1000 (optional, default : 1)
    -   `options.transparency` **[Number][8]** change the transparency of model - float 0 to 1 (optional, default : 1)
-   `onSuccess` **[Function][2]?** called on completion with no argument.
-   `onError` **[Function][2]?** called on fail to load with no argument. Error due to invalid RGB color

Example:

	var url = 'https://tailormate3d.com/{instanceName}/texture/{referenceId}';
	var options = {};
    viewport.loadColorBySceneId(212,112,32,'spread_collar',options,function(){
    	//Call on success	
	}, function(error){
		//Call on error
	});


## loadColorByName

Render the RGB color on specific scene object

#### Parameters

-   `r` **[Number][8]?** Red color of RGB - Integer 0 to 255 (required)
-   `g` **[Number][8]?** Green color of RGB - Integer 0 to 255 (required)
-   `b` **[Number][8]?** Blue color of RGB - Integer 0 to 255 (required)
-   `sceneId` **[String][1]?** Object id
-   `options` **[Object][5]?** (required in case of callback)
    -   `options.shininess` **[Number][8]** Increase/Decrease shine - float 1 to 1000 (optional, default : 1)
    -   `options.transparency` **[Number][8]** change the transparency of model - float 0 to 1 (optional, default : 1)
-   `onSuccess` **[Function][2]?** called on completion with no argument.
-   `onError` **[Function][2]?** called on fail to load with no argument. Error due to invalid RGB color

Example:

	var url = 'https://tailormate3d.com/{instanceName}/texture/{referenceId}';
	var options = {};
    viewport.loadColorByName(212,112,32,'spread_collar',options,function(){
    	//Call on success	
	}, function(error){
		//Call on error
	});


[1]: https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/String

[2]: https://developer.mozilla.org/docs/Web/JavaScript/Reference/Statements/function

[3]: https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Error

[4]: https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Promise

[5]: https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Object

[6]: https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Boolean

[7]: https://developer.mozilla.org/en-US/docs/Web/API/HTMLElement

[8]: https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Number

[9]: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array
