In order to use the data API you need an API key. This API key can be found on the account page if you have the right to use API or contact administrator.

You have to use the API key in querystring(?apiKey=1234).

    https://tailormate3d.com/{instanceName}/api/v1/{documentType}

**Document Types**

* `scenes`
* `designs`

## designs

### - GET /stream/{Offset}/{Limit}

Returns a list of entities

The default values are:

    Offset = 0
    Limit = 30

You can use following query parameters with stream

* search (string)
:  `/api/v1/designs/stream?search={searchTerms}`
* labelids (Multiple numeric ids for the same name need to be splitted by ,)
:  `/api/v1/designs/stream?labelids=1,2,3`
* order (orderType "a"(ascending order) or "d"(descending order), default value is "d")
:  `/api/v1/designs/stream?order={orderType}`

##### Request (http)

    https://tailormate3d.com/{instanceName}/api/v1/designs/stream/0/30?apikey=xxxx

##### Response (json)

    {
	    "status": true,
	    "data": {
	        "values": [{
	            "Id": 1,
	            "Name": "design-1",
	            "Description": null,
	            "FileName": "design-1.jpg",
	            "FileWidth": 1115,
	            "FileHeight": 1080,
	            "FileSize": "494.51",
	            "ReferenceId": "design-1.jpg",
	            "Url": "http://tailormate3d.com/{instanceName}/api/v1/designs/image/design-1.jpg?width=300"
	        }, {
	            "Id": 423,
	            "Name": "design-2",
	            "Description": null,
	            "FileName": "design-2.jpg",
	            "FileWidth": 1080,
	            "FileHeight": 1080,
	            "FileSize": "656.53",
	            "ReferenceId": "design-2.jpg",
	            "Url": "http://tailormate3d.com/{instanceName}/api/v1/designs/image/design-2.jpg?width=300"
	        }],
	        "count": 2
	    }
	}

### - GET Image /designs/image/{referenceId}

Parameters

* width (integer) - optional
* height (integer) - optional
    
Example: 

    https://tailormate3d.com/{instanceName}/api/v1/designs/image/{referenceId}?width={number}&height={number}


### - GET /labels/designs

Get a list of labels for document type designs

##### Request (http)
    https://tailormate3d.com/{instanceName}/api/v1/labels/designs/?apikey=xxxx

##### Response (json)
    {
	    "status": true,
	    "data": [{
	        "Id": 1,
	        "Name": "Tailormate designs",
	        "Parent": 0
	    }, {
	        "Id": 2,
	        "Name": "type1",
	        "Parent": 1
	    }, {
	        "Id": 3,
	        "Name": "XYZ Designs",
	        "Parent": 0
	    }]
	}

### - GET /labels/designs/{parentId}

Returns a list of child labels of particular parent.

##### Request (http)
    https://tailormate3d.com/{instanceName}/api/v1/labels/designs/1/?apikey=xxxx

##### Response (json)
    {
	    "status": true,
	    "data": [{
	        "Id": 2,
	        "Name": "type1",
	        "Parent": 1
	    }]
	}


## scenes

Scene is the 3d object which will render on viewport. Objects are the separate part of the scene.

### - GET /stream/{Offset}/{Limit}

Returns a list of entities

The default values are:

    Offset = 0
    Limit = 30

You can use following query parameters with stream

* search (string)
:  `/api/v1/scenes/stream?search={searchTerms}`
* labelids (Multiple numeric ids for the same name need to be splitted by ,)
:  `/api/v1/scenes/stream?labelids=1,2,3`
* order (orderType "a"(ascending order) or "d"(descending order), default value is "d")
:  `/api/v1/scenes/stream?order={orderType}`

##### Request (http)

    https://tailormate3d.com/{instanceName}/api/v1/scenes/stream/0/30?apikey=xxxx

##### Response (json)

    {
	    "status": true,
	    "data": {
	        "values": [{
	            "Id": 1,
	            "Name": "Spread Collar",
	            "ImageName": null,
	            "ReferenceId": "spread_collar",
	            "Url": "",
	            "Objects": [{
	                "Name": "spread collar inner",
	                "ReferenceId": "spread_collar_inner",
	                "imageUrl": "http://tailormate3d.com/{instanceName}/api/v1/scenes/image/spread_collar_inner.jpg"
	            },{
	                "Name": "spread collar outer",
	                "ReferenceId": "spread_collar_outer",
	                "imageUrl": "http://tailormate3d.com/{instanceName}/api/v1/scenes/image/spread_collar_outer.jpg"
	            }]
	        }],
	        "count": 10
	    }
	}